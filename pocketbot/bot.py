import sys
import traceback
import discord
from discord.ext import commands

bot = commands.Bot(command_prefix="!")


extensions = ['cogs.invention']


for extension in extensions:
    try:
        bot.load_extension(extension)
    except Exception as e:
        print(f'Failed to load {extension}', file=sys.stderr)
        traceback.print_exc()


@bot.event
async def on_ready():
    print(f'\n\nLogged in as: {bot.user.name} - {bot.user.id}\nVersion: {discord.__version__}\n')
    await bot.change_presence(status="Treasure Hunter")


with open("token", "r") as token:
    data = token.readlines().pop()
    bot.run(data, reconnect=True)
