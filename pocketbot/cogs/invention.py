import discord
from discord.ext import commands
from pyrspb.parsers import MaterialCalculator
from pyrspb.objects import InventionItem
from prettytable import PrettyTable


def item_to_list(item: InventionItem):
    return [item.item_name, item.buy_limit, item.materials_per_hour, item.cost_per_material]


class InventionCog:
    def __init__(self, bot: discord.Client):
        self.bot: discord.Client = bot

    @commands.command()
    async def component(self, ctx: commands.Context, *, material: str):
        items = MaterialCalculator(material).ideal_items[:5]
        table = PrettyTable(["Name", "Buy limit", "Materials/hr", "Gp/material"])
        for item in items:
            table.add_row(item_to_list(item))
        await ctx.send(str("```\n" + str(table) + "```"))


def setup(bot):
    bot.add_cog(InventionCog(bot))
